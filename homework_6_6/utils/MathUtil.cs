using System;
using System.Collections.Generic;
using System.Linq;

namespace homework_6_6.utils
{
    public static class MathUtil
    {
        public static int CalcGroupsCountOnly(int number)
        {
            return (int) Math.Log(number, 2) + 1;
        }

        public static List<List<int>> CalcGroups(int number)
        {
            var groups = new List<List<int>>();

            groups.Add(new List<int>(1) {1});

            if (number == 1)
                return groups;

            var numbers = Enumerable.Range(2, number - 1).ToList();

            while (numbers.Any())
            {
                var group = numbers.ToList();
                
                for (var i = 0; i < group.Count; i++)
                    group.RemoveAll(num => num != group[i] && num % group[i] == 0);
                
                groups.Add(group);
                numbers.RemoveAll(num => group.Contains(num));
            }

            return groups;
        }
    }
}