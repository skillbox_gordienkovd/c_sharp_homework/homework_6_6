using System.Collections.Generic;
using System.IO;
using System.IO.Compression;

namespace homework_6_6.utils
{
    public static class FileUtil
    {
        public static void WriteGroupsInFileWithoutArchive(List<List<int>> groups, out long fileSizeWithoutArchive)
        {
            if (File.Exists(@"d:\groups.txt")) File.Delete(@"d:\groups.txt");

            using var streamWriter = new StreamWriter(@"d:\groups.txt");

            for (var i = 0; i < groups.Count; i++) streamWriter.WriteLine($"Группа {i}: {string.Join(",", groups[i])}");

            fileSizeWithoutArchive = new FileInfo(@"d:\groups.txt").Length;
        }

        public static void WriteGroupsInFileWithArchive(List<List<int>> groups, out long fileSizeWithArchive)
        {
            if (File.Exists(@"d:\groups.zip")) File.Delete(@"d:\groups.zip");

            WriteGroupsInFileWithoutArchive(groups, out var file);

            using var zip = ZipFile.Open(@"d:\groups.zip", ZipArchiveMode.Create);
            zip.CreateEntryFromFile(@"d:\groups.txt", "groups.txt");

            File.Delete(@"d:\groups.txt");

            fileSizeWithArchive = new FileInfo(@"d:\groups.zip").Length;
        }

        public static void WriteGroupsInFileWithAndWithoutArchive(List<List<int>> groups, out long fileSizeWithArchive,
            out long fileSizeWithoutArchive)
        {
            if (File.Exists(@"d:\groups.zip")) File.Delete(@"d:\groups.zip");

            if (File.Exists(@"d:\groups.txt")) File.Delete(@"d:\groups.txt");

            var streamWriter = new StreamWriter(@"d:\groups.txt");

            for (var i = 0; i < groups.Count; i++) streamWriter.WriteLine($"Группа {i}: {string.Join(",", groups[i])}");

            streamWriter.Flush();
            streamWriter.Close();

            fileSizeWithoutArchive = new FileInfo(@"d:\groups.txt").Length;

            using var zip = ZipFile.Open(@"d:\groups.zip", ZipArchiveMode.Create);
            zip.CreateEntryFromFile(@"d:\groups.txt", "groups.txt");

            fileSizeWithArchive = new FileInfo(@"d:\groups.zip").Length;
        }
    }
}