using System;
using System.Collections.Generic;
using homework_6_6.utils;

namespace homework_6_6
{
    public class MathCalcSystem
    {
        public static void Start()
        {
            var userNumber = ConsoleUtil.ReadUserNumber();
            var actionType = ConsoleUtil.ReadUserActonType();
            ExecuteAction(userNumber, actionType);
        }

        private static void ExecuteAction(int number, ActionType actionType)
        {
            List<List<int>> groups;
            long fileSizeWithoutArchive;
            long fileSizeWithArchive;

            switch (actionType)
            {
                case ActionType.COUNT_GROUPS_ONLY:
                    var groupsCount = MathUtil.CalcGroupsCountOnly(number);
                    ConsoleUtil.WriteGroupsCount(groupsCount, number);
                    break;
                case ActionType.WITHOUT_ARCHIVE:
                    groups = MathUtil.CalcGroups(number);
                    FileUtil.WriteGroupsInFileWithoutArchive(groups, out fileSizeWithoutArchive);
                    ConsoleUtil.WriteFileSizeWithoutArchive(fileSizeWithoutArchive);
                    break;
                case ActionType.WITH_ARCHIVE:
                    groups = MathUtil.CalcGroups(number);
                    FileUtil.WriteGroupsInFileWithArchive(groups, out fileSizeWithArchive);
                    ConsoleUtil.WriteFileSizeWithArchive(fileSizeWithArchive);
                    break;
                case ActionType.WITH_ARCHIVE_AND_WITHOUT_ARCHIVE:
                    groups = MathUtil.CalcGroups(number);
                    FileUtil.WriteGroupsInFileWithAndWithoutArchive(groups, out fileSizeWithArchive,
                        out fileSizeWithoutArchive);
                    ConsoleUtil.WriteFileSizeWithAndWithoutArchive(fileSizeWithArchive, fileSizeWithoutArchive);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(actionType), actionType, "Неизвестный тип действия");
            }

            ConsoleUtil.EndProgram();
        }
    }
}