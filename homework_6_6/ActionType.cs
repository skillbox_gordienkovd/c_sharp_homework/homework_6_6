namespace homework_6_6
{
    public enum ActionType
    {
        COUNT_GROUPS_ONLY,
        WITH_ARCHIVE,
        WITHOUT_ARCHIVE,
        WITH_ARCHIVE_AND_WITHOUT_ARCHIVE
    }
}