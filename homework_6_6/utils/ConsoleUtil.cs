using System;

namespace homework_6_6.utils
{
    public static class ConsoleUtil
    {
        public static int ReadUserNumber()
        {
            Console.WriteLine("Введите число от 1 до 1 000 000 000 включительно");
            while (true)
            {
                var success = int.TryParse(Console.ReadLine(), out var result);

                if (success && result is >= 1 and <= 1_000_000_000)
                    return result;
                Console.Write("Число введено неверно.");
            }
        }

        public static ActionType ReadUserActonType()
        {
            Console.WriteLine("Выберите тип действия (нажмите клавищу от 1 до 4): \n" +
                              "1 - Только показать количество групп.\n" +
                              "2 - Сохранить результат в файл без архивации\n" +
                              "3 - Сохранить результат в файл с архивацией\n" +
                              "4 - Сохранить результат в файлы с архивацией и без архивации\n");

            while (true)
            {
                var key = Console.ReadKey().Key;

                switch (key)
                {
                    case ConsoleKey.D1:
                    case ConsoleKey.NumPad1:
                        return ActionType.COUNT_GROUPS_ONLY;
                    case ConsoleKey.D2:
                    case ConsoleKey.NumPad2:
                        return ActionType.WITHOUT_ARCHIVE;
                    case ConsoleKey.D3:
                    case ConsoleKey.NumPad3:
                        return ActionType.WITH_ARCHIVE;
                    case ConsoleKey.D4:
                    case ConsoleKey.NumPad4:
                        return ActionType.WITH_ARCHIVE_AND_WITHOUT_ARCHIVE;
                    default:
                        continue;
                }
            }
        }

        public static void WriteGroupsCount(int groupsCount, int number)
        {
            Console.WriteLine($"\nДля числа {number} количество групп: {groupsCount}");
        }

        public static void EndProgram()
        {
            Console.WriteLine("\nДля завершения программы нажмите любую клавишу");
            Console.ReadKey();
        }

        public static void WriteFileSizeWithoutArchive(long fileSizeWithoutArchive)
        {
            Console.WriteLine($"\nРазмер сохраненного файла c группами (groups.txt): {fileSizeWithoutArchive} байт");
        }

        public static void WriteFileSizeWithArchive(long fileSizeWithArchive)
        {
            Console.WriteLine(
                $"\nРазмер сохраненного в архив файла c группами (groups.txt.zip): {fileSizeWithArchive} байт");
        }

        public static void WriteFileSizeWithAndWithoutArchive(long fileSizeWithArchive, long fileSizeWithoutArchive)
        {
            Console.WriteLine($"\nРазмер сохраненного файла c группами (groups.txt): {fileSizeWithoutArchive} байт");
            Console.WriteLine(
                $"Размер сохраненного в архив файла c группами (groups.txt.zip): {fileSizeWithArchive} байт");
        }
    }
}